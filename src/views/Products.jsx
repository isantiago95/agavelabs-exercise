import React from 'react';
import Slide from '../components/Slide';
import watch from '../assets/img/watch.png';
import { Row, Col, Button, Input } from 'reactstrap';
import { connect } from 'react-redux';
import { filterProducts } from '../helpers/utils';
import ListProducts from '../components/ListProducts';

const Products = ({ products, categories }) => {
  const [list, setList] = React.useState([]);

  React.useEffect(() => {
    setList(products);
  }, [products]);

  const filterByCategory = (item) => {
    if (item === 'all') {
      setList(products);
    } else {
      const array = products.filter((product) => product.category === item);
      setList(array);
    }
  };

  const searchProduct = (target) => {
    const { value } = target;
    const filter = filterProducts(products, value);
    setList(filter);
  };

  return (
    <React.Fragment>
      <Slide title='Products' image={watch} subtitle='Find the right product' />

      <Row>
        <Col md='4' className='filters'>
          <div>
            <div>
              <h4>Categories</h4>
              <ul>
                {categories &&
                  categories.map((item, key) => (
                    <Button
                      key={key}
                      className='btn-filter-link'
                      color='link'
                      onClick={() => filterByCategory(item)}>
                      <li>{item}</li>
                    </Button>
                  ))}
                <Button
                  className='btn-filter-link'
                  color='link'
                  onClick={() => filterByCategory('all')}>
                  <li>All products</li>
                </Button>
              </ul>
            </div>
            <div>
              <h4>Search by category or product</h4>
              <Input
                type='text'
                placeholder='Search by category or product'
                onChange={({ target }) => searchProduct(target)}
              />
            </div>
          </div>
        </Col>
        <Col>
          <div className='mt-5'>
            <ListProducts products={list} />
          </div>
        </Col>
      </Row>
    </React.Fragment>
  );
};

const mapStateToProps = (state) => {
  const { products, categories } = state.store;
  return { products, categories };
};
export default connect(mapStateToProps, {})(Products);
