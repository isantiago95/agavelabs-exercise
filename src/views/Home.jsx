import React from 'react';
import { Row, Col, Container } from 'reactstrap';
import { connect } from 'react-redux';
import Headphones from '../assets/img/headphones.png';
import Slide from '../components/Slide';
import ListProducts from '../components/ListProducts';

const Home = ({ products }) => {
  return (
    <React.Fragment>
      <Slide title='Fake Store' image={Headphones} subtitle='Buy anything' />

      <Container className='mt-5'>
        <Row>
          <Col>
            <ListProducts products={products} />
          </Col>
        </Row>
      </Container>
    </React.Fragment>
  );
};

const mapStateToProps = (state) => {
  const { products } = state.store;
  return { products };
};
export default connect(mapStateToProps, {})(Home);
