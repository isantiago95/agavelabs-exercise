import React from 'react';
import './assets/scss/index.scss';
import Routes from './routes/index';

function App() {
  return <Routes />;
}

export default App;
