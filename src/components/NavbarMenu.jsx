import React from 'react';
import { connect } from 'react-redux';
import {
  Collapse,
  Nav,
  Navbar,
  NavbarBrand,
  NavbarToggler,
  NavItem,
  NavLink,
  Button,
} from 'reactstrap';
import Cart from './Cart';

const NavbarMenu = ({ cart }) => {
  const [openCart, setOpenCart] = React.useState(false);
  const [itemsQty, setItemsQty] = React.useState(0);
  const [openMenu, setOpenMenu] = React.useState(false);

  React.useEffect(() => {
    if (cart.length > 0) {
      const qty = cart.map((item) => item.qty).reduce((count, curr) => count + curr);
      setItemsQty(qty);
    } else {
      setItemsQty(0);
    }
  }, [cart]);

  const handleCart = () => setOpenCart(!openCart);
  return (
    <React.Fragment>
      <Navbar color='dark' dark expand='md' fixed='top' light>
        <NavbarBrand href='/'>Fake Store</NavbarBrand>
        <Button color='dark' outline className='cart-button-sm' onClick={handleCart}>
          <img src={require('../assets/img/bag.png')} alt='cart' className='cart-icon' />
          <div className='cart-qty'>{itemsQty}</div>
        </Button>
        <NavbarToggler onClick={() => setOpenMenu(!openMenu)} />
        <Collapse isOpen={openMenu} navbar>
          <Nav className='me-auto' navbar>
            <NavItem>
              <NavLink href='/products'>Products</NavLink>
            </NavItem>
          </Nav>

          <Button color='dark' outline className='cart-button-lg' onClick={handleCart}>
            <img src={require('../assets/img/bag.png')} alt='cart' className='cart-icon' />
            <div className='cart-qty'>{itemsQty}</div>
          </Button>
        </Collapse>
      </Navbar>
      {openCart && <Cart open={openCart} close={handleCart} />}
    </React.Fragment>
  );
};

const mapStateToProps = (state) => {
  const { cart } = state.store;
  return { cart };
};

export default connect(mapStateToProps, {})(NavbarMenu);
