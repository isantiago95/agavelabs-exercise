import React from 'react';
import { Card, CardImg, CardImgOverlay, CardTitle } from 'reactstrap';
import { Link } from 'react-router-dom';
import ProductModal from './ProductModal';

const CategoryCard = ({ item }) => {
  const [open, setOpen] = React.useState(false);

  const openModal = () => setOpen(!open);

  return (
    <React.Fragment>
      {open && <ProductModal close={openModal} item={item} open={open} />}
      <Link to={`/categories/${item}`}>
        <Card inverse>
          <CardImg alt='Card image cap' src='https://picsum.photos/318/270' width='100%' />
          <CardImgOverlay className='d-flex justify-content-center align-items-center'>
            <CardTitle tag='h2'>{item}</CardTitle>
          </CardImgOverlay>
        </Card>
      </Link>
    </React.Fragment>
  );
};

export default CategoryCard;
