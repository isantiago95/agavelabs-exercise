import React from 'react';
import {
  Button,
  Col,
  Form,
  FormGroup,
  Input,
  Label,
  Modal,
  ModalBody,
  ModalHeader,
  Row,
} from 'reactstrap';
import 'react-credit-cards/lib/styles.scss';
import Cards from 'react-credit-cards';
import { checkout } from '../redux/actions';
import { connect } from 'react-redux';
import { validateExpiration } from '../helpers/utils';
import AlertModal from './AlertModal';

const Checkout = ({ open = false, close = () => {}, cart, checkout }) => {
  const [card, setCard] = React.useState({
    cvc: '',
    expiry: '',
    focus: '',
    name: '',
    number: '',
    email: '',
  });
  const [total] = React.useState(
    cart.length > 0 && cart.map((p) => p.qty * p.price).reduce((sum, curr) => sum + curr)
  );
  const [error, setError] = React.useState(null);
  const [openAlert, setOpenAlert] = React.useState(false);

  React.useEffect(() => {
    const date = `${card.expiry.slice(0, 2)}/${card.expiry.slice(2)}`;
    if (card.expiry.length === 4 && !card.expiry.includes('/')) setCard({ ...card, expiry: date });
    if (card.expiry.length === 5 && !validateExpiration(card.expiry))
      setError('Expiration date invalid');
    else setError(null);
  }, [card]);

  const handleInputFocus = (e) => {
    setCard({ ...card, focus: e.target.name });
  };

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    if (name === 'number' && value.length === 17) return;
    if (name === 'expiry' && value.length === 6) return;
    if (name === 'cvc' && value.length === 4) return;
    setCard({ ...card, [name]: value });
  };

  const submit = (e) => {
    e.preventDefault();
    if (error) return;
    setOpenAlert(true);
    checkout({ card, cart, total });
  };

  const handleClose = () => {
    setOpenAlert(false);
    close();
  };

  return (
    <Modal isOpen={open} centered toggle={close}>
      {openAlert && <AlertModal open={openAlert} close={handleClose} />}
      <ModalHeader toggle={close}>Checkout</ModalHeader>
      <ModalBody>
        <h4 className='text-center mb-4'>
          Total order: <span className='text-success'>${total}</span>
        </h4>
        <Cards
          cvc={card.cvc}
          expiry={card.expiry}
          focused={card.focus}
          name={card.name}
          number={card.number}
        />
        <p className='text-center text-info mt-3 mx-5'>
          Use any card number combination starting with 4 or 5 to see the magic...
        </p>
        <Form onSubmit={submit}>
          <Row form className='mt-4'>
            <Col>
              <FormGroup floating>
                <Input
                  required
                  id='name'
                  name='name'
                  placeholder='Name on card'
                  type='name'
                  onChange={handleInputChange}
                  onFocus={handleInputFocus}
                  value={card.name}
                />
                <Label for='name'>Name on card</Label>
              </FormGroup>
            </Col>
            <Col>
              <FormGroup floating>
                <Input
                  required
                  id='number'
                  name='number'
                  placeholder='Card number'
                  type='number'
                  onChange={handleInputChange}
                  onFocus={handleInputFocus}
                  value={card.number}
                />
                <Label for='number'>Card number</Label>
              </FormGroup>
            </Col>
          </Row>

          <Row>
            <Col md={6}>
              <FormGroup floating>
                <Input
                  required
                  id='expiry'
                  name='expiry'
                  placeholder='MM/AA'
                  type='text'
                  onChange={handleInputChange}
                  onFocus={handleInputFocus}
                  value={card.expiry}
                />
                <Label for='expiry'>MM/AA</Label>
                {error && <p className='text-danger'>{error}</p>}
              </FormGroup>
            </Col>
            <Col md={6}>
              <FormGroup floating>
                <Input
                  required
                  id='cvc'
                  name='cvc'
                  placeholder='CVC'
                  type='text'
                  onChange={handleInputChange}
                  onFocus={handleInputFocus}
                  value={card.cvc}
                />
                <Label for='cvc'>CVC</Label>
              </FormGroup>
            </Col>
          </Row>

          <Row form>
            <Col>
              <FormGroup floating>
                <Input
                  required
                  id='email'
                  name='email'
                  placeholder='Email'
                  type='email'
                  onChange={handleInputChange}
                  onFocus={handleInputFocus}
                  value={card.email}
                />
                <Label for='email'>Email</Label>
              </FormGroup>
            </Col>
          </Row>
          <Button type='submit' color='primary' block>
            Pay
          </Button>
        </Form>
      </ModalBody>
    </Modal>
  );
};

export default connect(null, { checkout })(Checkout);
