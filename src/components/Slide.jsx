import React from 'react';
import { Row, Col } from 'reactstrap';

const Slide = ({ title, image, subtitle }) => {
  return (
    <Row className='main-slide'>
      <Col className='d-flex flex-column align-items-center justify-content-center'>
        <h1 className='display-1'>{title}</h1>
        <h3>{subtitle}</h3>
      </Col>
      <Col>
        <img
          src={image}
          className='main-product'
          alt='headphones'
          data-aos='fade-left'
          data-aos-duration='2000'
        />
      </Col>
    </Row>
  );
};

export default Slide;
