import React from 'react';
import { Button, Card, CardBody, CardFooter, CardImg, CardSubtitle, CardTitle } from 'reactstrap';
import ProductModal from '../components/ProductModal';

const ProductCard = ({ item }) => {
  const [open, setOpen] = React.useState(false);

  const openModal = () => setOpen(true);
  const closeModal = () => setOpen(false);

  return (
    <React.Fragment>
      {open && <ProductModal close={closeModal} item={item} open={open} />}
      <Card key={item.id} body className='product-card text-center'>
        <CardImg className='image-product' alt='Card image cap' src={item.image} top />
        <CardBody>
          <CardTitle tag='h5'>{item.title}</CardTitle>
          <CardSubtitle className='mb-2 text-success' tag='h4'>
            ${item.price}
          </CardSubtitle>
        </CardBody>
        <CardFooter className='bg-transparent border-top-0'>
          <Button outline color='primary' onClick={openModal}>
            See Details
          </Button>
        </CardFooter>
      </Card>
    </React.Fragment>
  );
};

export default ProductCard;
