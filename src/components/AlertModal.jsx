import React from 'react';
import { Button, Modal, ModalBody, ModalFooter, ModalHeader, Spinner } from 'reactstrap';
import { clearMessages } from '../redux/actions';
import { connect } from 'react-redux';
import errorIcon from '../assets/img/error-icon.svg';
import successIcon from '../assets/img/success-icon.svg';
import infoIcon from '../assets/img/info-icon.svg';

const AlertModal = ({ close = () => {}, open = false, success, error, loading, clearMessages }) => {
  const toggleCloseModal = () => {
    close();
    clearMessages();
  };

  return (
    <React.Fragment>
      <Modal centered backdrop='static' isOpen={open} toggle={toggleCloseModal}>
        <ModalHeader toggle={loading ? null : toggleCloseModal}>
          {success ? 'Success' : error ? 'Something went wrong' : loading && 'Loading...'}
        </ModalHeader>
        <ModalBody className='text-center'>
          {loading ? (
            <Spinner color='primary' children={null} />
          ) : (
            <img
              className='notification-icon'
              src={success ? successIcon : error ? errorIcon : loading && infoIcon}
              alt='notification-icon'
            />
          )}
          <div
            className={
              success
                ? 'mt-3 text-success'
                : error
                ? 'mt-3 text-danger'
                : loading && 'mt-3 text-info'
            }>
            <h4>{success ? success : error ? error : loading && 'Please wait...'}</h4>
          </div>
        </ModalBody>
        <ModalFooter>
          {!loading && (
            <Button color='primary' onClick={toggleCloseModal}>
              {success ? 'Done' : error && 'Close'}
            </Button>
          )}
        </ModalFooter>
      </Modal>
    </React.Fragment>
  );
};

const mapStateToProps = (state) => {
  const { error, loading, success } = state.store;
  return { error, loading, success };
};

export default connect(mapStateToProps, { clearMessages })(AlertModal);
