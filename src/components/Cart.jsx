import React from 'react';
import { connect } from 'react-redux';
import {
  Badge,
  Button,
  ListGroup,
  ListGroupItem,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
} from 'reactstrap';
import emptyCart from '../assets/img/empty-cart.png';
import Checkout from './Checkout';
import { removeFromCart, addToCart, clearMessages } from '../redux/actions';

const Cart = ({
  open = false,
  close = () => {},
  cart,
  removeFromCart,
  addToCart,
  clearMessages,
}) => {
  const [openCheckout, setOpenCheckout] = React.useState(false);
  const closeCart = () => {
    clearMessages();
    close();
  };

  const checkout = () => setOpenCheckout(!openCheckout);

  return (
    <Modal isOpen={open} centered size='lg' toggle={closeCart}>
      {openCheckout && <Checkout open={openCheckout} close={closeCart} cart={cart} />}
      <ModalHeader toggle={closeCart}>Cart</ModalHeader>
      <ModalBody>
        {cart.length > 0 ? (
          <ListGroup>
            {cart.map((item) => (
              <ListGroupItem key={item.id} className='d-lg-flex justify-content-between'>
                <div className='my-2'>
                  <img src={item.image} alt='' style={{ width: '2em' }} className='me-2' />{' '}
                  {item.title.length > 40 ? item.title.substring(0, 30) : item.title}
                </div>
                <div className='my-2 d-flex align-items-center justify-content-end'>
                  <span className='text-success me-3'>${item.price} each</span>
                  <Button
                    size='sm'
                    color='danger'
                    outline
                    onClick={() => removeFromCart({ cart, product: item })}>
                    -
                  </Button>
                  <Badge color='primary' pill className='mx-2'>
                    {item.qty}
                  </Badge>
                  <Button
                    size='sm'
                    color='success'
                    outline
                    onClick={() => addToCart({ cart, product: item })}>
                    +
                  </Button>
                </div>
              </ListGroupItem>
            ))}
          </ListGroup>
        ) : (
          <div className='text-center'>
            <img src={emptyCart} alt='empty' />
            <h3>Your cart is empty</h3>
          </div>
        )}
      </ModalBody>
      <ModalFooter>
        {cart.length > 0 && (
          <React.Fragment>
            <h2 className='text-success'>
              ${cart.map((p) => p.qty * p.price).reduce((sum, curr) => sum + curr)}
            </h2>
            <Button color='primary' onClick={checkout}>
              Checkout
            </Button>
          </React.Fragment>
        )}
        <Button onClick={closeCart}>Close</Button>
      </ModalFooter>
    </Modal>
  );
};

const mapStateToProps = (state) => {
  const { cart } = state.store;
  return { cart };
};

export default connect(mapStateToProps, { removeFromCart, addToCart, clearMessages })(Cart);
