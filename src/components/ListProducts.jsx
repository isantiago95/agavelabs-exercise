import React from 'react';
import ProductCard from './ProductCard';

const ListProducts = ({ products }) => {
  return (
    <ul className='subscribe-cards'>
      {products &&
        products.map((item) => (
          <li key={item.id}>
            <ProductCard item={item} />
          </li>
        ))}
    </ul>
  );
};

export default ListProducts;
