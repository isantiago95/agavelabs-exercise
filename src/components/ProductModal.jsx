import React from 'react';
import { Button, Modal, ModalBody, ModalFooter, Row, Col } from 'reactstrap';
import { addToCart, clearMessages } from '../redux/actions';
import { connect } from 'react-redux';
import starIcon from '../assets/img/star.png';

const ProductModal = ({
  open = false,
  close = () => {},
  item,
  cart,
  success = '',
  addToCart,
  clearMessages,
}) => {
  const [itemsQty, setItemsQty] = React.useState(0);

  React.useEffect(() => {
    const exists = cart.filter((thisItem) => thisItem.id === item.id)[0];
    if (exists) {
      setItemsQty(exists.qty);
    }
  }, [cart]);

  const closeModal = () => {
    close();
    clearMessages();
  };

  return (
    <Modal className='product-modal' isOpen={open} centered toggle={closeModal}>
      <ModalBody className='d-flex flex-column'>
        <h4>{item.title}</h4>
        <div className='py-2'>
          {item.rating.rate} <img src={starIcon} alt='rating' className='rate-icon' />
        </div>
        <p className='py-2'>Category: {item.category}</p>
        <img src={item.image} alt='product' className='product-image align-self-center' />
        <div className='pt-4'>{item.description}</div>
      </ModalBody>
      <ModalFooter className='d-flex justify-content-around text-center'>
        <Row>
          <Col>
            <h3 className='text-success'>${item.price}</h3>
            <div>
              {success && <p className='text-success'>{`${success} - ${itemsQty}`}</p>}
              <Button color='primary' onClick={() => addToCart({ product: item, cart })}>
                Add to cart
              </Button>
            </div>
          </Col>
        </Row>
      </ModalFooter>
    </Modal>
  );
};

const mapStateToProps = (state) => {
  const { cart, success } = state.store;
  return { cart, success };
};
export default connect(mapStateToProps, { addToCart, clearMessages })(ProductModal);
