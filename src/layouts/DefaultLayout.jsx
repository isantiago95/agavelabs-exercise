import React from 'react';
import { Container, Row, Col } from 'reactstrap';

import MainNavbar from '../components/NavbarMenu';

const DefaultLayout = ({ children }) => (
  <Container fluid>
    <Row>
      <Col className='main-content p-0' sm='12' tag='main'>
        <MainNavbar />
        <div className='content'>{children}</div>
      </Col>
    </Row>
  </Container>
);

export default DefaultLayout;
