import React, { Suspense } from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

//import routes
import { publicRoutes } from './routes';

import DefaultLayout from '../layouts/DefaultLayout';

// handle auth and authorization
const AppRoute = ({ component: Component, layout: Layout, ...rest }) => {
  return (
    <Route
      {...rest}
      render={(props) => {
        // authorized so return component
        return (
          <Layout>
            <Component {...props} />
          </Layout>
        );
      }}
    />
  );
};

/**
 * Main Route component
 */
const Routes = (props) => {
  return (
    // rendering the router with layout
    <BrowserRouter>
      <React.Fragment>
        <Suspense fallback={<div />}>
          <Switch>
            {/* public routes */}
            {publicRoutes.map((route, idx) => (
              <AppRoute
                path={route.path}
                layout={DefaultLayout}
                component={route.component}
                key={idx}
              />
            ))}
          </Switch>
        </Suspense>
      </React.Fragment>
    </BrowserRouter>
  );
};

export default Routes;
