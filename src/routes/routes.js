import React from 'react';
import { Redirect } from 'react-router-dom';

const Home = React.lazy(() => import('../views/Home'));
const Products = React.lazy(() => import('../views/Products'));

const publicRoutes = [
  { path: '/home', component: Home },
  { path: '/products', component: Products },
  {
    path: '/',
    exact: true,
    component: () => <Redirect to='/home' />,
  },
];

export { publicRoutes };
