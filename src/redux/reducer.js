import * as types from './constants';

export const initialState = {
  loading: false,
  cart: [],
};

const storeReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.LOADING: {
      return {
        ...state,
        loading: true,
      };
    }
    case types.RELOAD_PAGE: {
      const { products, categories, cart } = action.payload;
      return { ...state, products, categories, cart, loading: false };
    }

    case types.ADD_TO_CART: {
      const { cart, success } = action.payload;
      return { ...state, cart, success };
    }
    case types.REMOVE_FROM_CART: {
      const { cart, success } = action.payload;
      return { ...state, cart, success };
    }

    case types.PAYMENT_SUCCESS: {
      return { ...state, loading: false, success: action.payload, cart: [] };
    }
    case types.ERROR: {
      return { ...state, loading: false, error: action.payload };
    }
    case types.CLEAR_MESSAGES: {
      delete state.error;
      delete state.success;
      return { ...state };
    }
    default:
      return state;
  }
};

export default storeReducer;
