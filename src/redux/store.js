import { createStore, combineReducers, compose, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import storeReducer from './reducer';
import { reloadPage } from './actions';

const rootReducer = combineReducers({
  store: storeReducer,
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default function generateStore() {
  const store = createStore(rootReducer, composeEnhancers(applyMiddleware(thunk)));
  reloadPage()(store.dispatch);
  return store;
}
