export const LOADING = 'LOADING';
export const RELOAD_PAGE = 'RELOAD_PAGE';
export const ADD_TO_CART = 'ADD_TO_CART';
export const REMOVE_FROM_CART = 'REMOVE_FROM_CART';
export const PAYMENT_SUCCESS = 'PAYMENT_SUCCESS';

export const ERROR = 'ERROR';
export const SUCCESS = 'SUCCESS';
export const CLEAR_MESSAGES = 'CLEAR_MESSAGES';
