import * as types from './constants';
import { getProducts, getCategories } from '../helpers/utils';
import { PRODUCTS_CATEGORIES, CART } from '../helpers/constants';
import { sendEmail } from '../helpers/email';
/**
 * load page
 * @param {*} payload
 */
export const reloadPage = () => async (dispatch) => {
  try {
    await dispatch({ type: types.LOADING });
    const dataFromStorage = JSON.parse(localStorage.getItem(PRODUCTS_CATEGORIES));
    const cartFromStorage = JSON.parse(localStorage.getItem(CART));

    if (!dataFromStorage) {
      const productsList = await getProducts();
      const categoriesList = await getCategories();

      if (productsList.status === 200 && categoriesList.status === 200) {
        const products = productsList.data;
        const categories = categoriesList.data;

        localStorage.setItem(PRODUCTS_CATEGORIES, JSON.stringify({ products, categories }));

        await dispatch({
          type: types.RELOAD_PAGE,
          payload: { products, categories, cart: cartFromStorage ? cartFromStorage : [] },
        });
      } else {
        dispatch({ type: types.ERROR, payload: 'There was an error getting the product list.' });
      }
    } else {
      const { products, categories } = dataFromStorage;
      await dispatch({
        type: types.RELOAD_PAGE,
        payload: { products, categories, cart: cartFromStorage ? cartFromStorage : [] },
      });
    }
  } catch (error) {
    console.error(error);
  }
};

export const addToCart =
  ({ cart, product }) =>
  async (dispatch) => {
    try {
      const newCart = Object.assign([], cart);
      const item = newCart.filter((item) => item.id === product.id);
      if (item.length === 0) {
        newCart.push({ ...product, qty: 1 });
      } else {
        const index = newCart.findIndex((item) => item.id === product.id);
        newCart.splice(index, 1, { ...product, qty: newCart[index].qty + 1 });
      }
      localStorage.setItem(CART, JSON.stringify(newCart));
      await dispatch({
        type: types.ADD_TO_CART,
        payload: { cart: newCart, success: 'Item added to cart.' },
      });
    } catch (error) {
      console.error(error);
    }
  };

export const removeFromCart =
  ({ cart, product }) =>
  async (dispatch) => {
    try {
      const newCart = Object.assign([], cart);
      const index = newCart.findIndex((item) => item.id === product.id);

      if (newCart[index].qty === 1) {
        newCart.splice(index, 1);
      } else {
        newCart.splice(index, 1, { ...product, qty: newCart[index].qty - 1 });
      }

      localStorage.setItem(CART, JSON.stringify(newCart));
      await dispatch({
        type: types.REMOVE_FROM_CART,
        payload: { cart: newCart, success: 'Item removed from cart.' },
      });
    } catch (error) {
      console.error(error);
    }
  };

export const checkout =
  ({ card, cart, total }) =>
  async (dispatch) => {
    try {
      await dispatch({ type: types.LOADING });
      const { name, email } = card;
      const items = cart.map((item) => item.title);

      const sendOrder = await sendEmail({ name, email, items, total });
      if (sendOrder.status === 200) {
        localStorage.removeItem(CART);
        await dispatch({
          type: types.PAYMENT_SUCCESS,
          payload: 'Thanks for your order, please check your email address.',
        });
      } else {
        await dispatch({
          type: types.ERROR,
          payload: 'Ups, there was an error processing your order.',
        });
      }
    } catch (error) {
      console.error(error);
    }
  };

export const clearMessages = () => (dispatch) => {
  dispatch({ type: types.CLEAR_MESSAGES });
};
