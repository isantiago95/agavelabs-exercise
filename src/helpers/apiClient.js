import axios from 'axios';

// default
axios.defaults.baseURL = process.env.REACT_APP_API_URL;

/**
 * Sets the headers depending if auth is needed
 * @param {*} token
 */
function setHeaders({ token, form = true }) {
  let headers = form
    ? { Accept: 'multipart/form-data', 'Content-Type': 'multipart/form-data' }
    : { Accept: 'application/json', 'Content-Type': 'application/json' };
  return token
    ? (headers = {
        ...headers,
        'x-access-token': token,
      })
    : headers;
}

export async function post({ url, data = null, token = null }) {
  try {
    return await axios.post(url, data, {
      headers: setHeaders({ token }),
    });
  } catch (error) {
    console.error(error);
  }
}

export const get = async ({ url, token = null }) => {
  try {
    return await axios.get(url, {
      headers: setHeaders({ token }),
    });
  } catch (error) {
    console.error('error on get method: ', error);
  }
};
