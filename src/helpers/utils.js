import { get } from './apiClient';

/* Functions to use on redux */
export const getProducts = async () => {
  const { status, data } = await get({ url: '/products' });
  return { status, data };
};

export const getCategories = async () => {
  const { status, data } = await get({ url: '/products/categories' });
  return { status, data };
};

export function filterProducts(products, value) {
  const filteredData = products.filter((item) => {
    const searchStr = value.toLowerCase();
    const product = item.title.toLowerCase().includes(searchStr);
    const category = item.category.toLowerCase().includes(searchStr);
    return product || category;
  });
  return filteredData;
}

// validate card expiration
export const validateExpiration = (expiration) => {
  const givenDate = expiration.split('/');
  const str = `20${givenDate[1]}/${givenDate[0]}/01`;
  const date = new Date(str);
  const today = new Date();
  return date > today;
};
