import emailjs from 'emailjs-com';

const SERVICE_ID = process.env.REACT_APP_SERVICE_ID;
const TEMPLATE_ID = process.env.REACT_APP_TEMPLATE_ID;
const USER_ID = process.env.REACT_APP_USER_ID;

export const sendEmail = async (values) => {
  return await emailjs.send(SERVICE_ID, TEMPLATE_ID, values, USER_ID);
};
