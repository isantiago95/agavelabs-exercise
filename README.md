# Agavelabs exercise

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

# WARNING: The project needs a .env file to work properly but in order to make the email send work, it needs some credentials that are not included on this repo. If try to run it on localhost the email service wont work at all. The online website is located at [FakeStore](https://fakestore.israelsantiago.com)

--

## For time issues the site is not fully responsive, only for mobile devices.

--

### Quick Start

- Install dependencies by running `yarn` or `npm install`.
- Please create a `.env` file in the root folder with the following propertie:
  REACT_APP_API_URL='https://fakestoreapi.com',
- Run `yarn dev` or `npm run dev` to start the local development server.
- 😎 **That's it!**

## Available Scripts

In the project directory, you can run:

### `npm dev`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for
more information.
